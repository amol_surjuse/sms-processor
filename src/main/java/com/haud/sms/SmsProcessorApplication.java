package com.haud.sms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class SmsProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmsProcessorApplication.class, args);
	}

}
