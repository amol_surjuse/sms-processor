
package com.haud.sms;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.haud.sms.model.Message;

@Service
public class SmsSenderService {
	private static final Logger log = LogManager.getLogger(SmsSenderService.class);

	@Async
	public void send(Message message) {
		try {
			log.info("sending SMS to : {}", message.getTo());
			// Construct data
			String apiKey = "apikey=" + "yourapiKey";

			String sender = "&sender=" + "TXTLCL";
			String numbers = "&numbers=" + message.getTo();

			// Send data
			HttpURLConnection conn = (HttpURLConnection) new URL("https://api.textlocal.in/send/?").openConnection();
			String data = apiKey + numbers + message.getBody() + sender;
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Length", Integer.toString(data.length()));
			conn.getOutputStream().write(data.getBytes("UTF-8"));
			final BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			final StringBuffer stringBuffer = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				stringBuffer.append(line);
			}
			rd.close();
			log.debug(message + "##########" + stringBuffer.toString());

		} catch (Exception e) {
			log.error("Exception in sending SMS {} ", e);
		}
	}
}
