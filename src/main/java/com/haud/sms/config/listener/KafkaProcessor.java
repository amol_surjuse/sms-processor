package com.haud.sms.config.listener;

import java.util.function.Consumer;

import org.apache.kafka.streams.kstream.KStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.KafkaTemplate;

import com.haud.sms.model.Message;
import com.haud.sms.validator.DestinationAdressValidator;

@Configuration
public class KafkaProcessor {
	Logger log = LogManager.getLogger(KafkaProcessor.class);
	@Autowired
	private KafkaTemplate<String, Message> kafkaTemplate;

	@Bean
	public Consumer<KStream<String, Message>> smsProcessor() {
		return kStream -> kStream.foreach((key, message) -> {
			log.info("Started processing message {}", message);
			if(DestinationAdressValidator.validate(message)) {
				kafkaTemplate.send("sms.send", message);
			} else {
				log.info("message is not allowed message {}", message);
				kafkaTemplate.send("sms.spam", message);
			}
			kafkaTemplate.send("sms.charging", message);
		});
	}
}
