package com.haud.sms.config.listener;

import java.util.function.Consumer;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.haud.sms.SmsSenderService;
import com.haud.sms.model.Message;

@Configuration
public class SendMessageProcessor {
	@Autowired
	private SmsSenderService senderService;
	
	@Bean
	public Consumer<KStream<String, Message>> sendSms() {
		return kStream -> kStream.foreach((key, message) -> {
			senderService.send(message);
		});
	}
}
