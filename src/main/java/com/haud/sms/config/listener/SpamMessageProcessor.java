package com.haud.sms.config.listener;

import java.util.function.Consumer;

import org.apache.kafka.streams.kstream.KStream;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.haud.sms.model.Message;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class SpamMessageProcessor {
	@Bean
	public Consumer<KStream<String, Message>> processSpamSms() {
		return kStream -> kStream.foreach((key, message) -> {
			log.info("Started processing spam message {}", message);
		});
	}
}
