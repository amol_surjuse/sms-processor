package com.haud.sms.validator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.haud.sms.model.Message;

public class DestinationAdressValidator {
	private static final Set<String> allowedDestinations = new HashSet<>(
			Arrays.asList("1234567890", "1234567891", "1234567892", "1234567893", "1234567894", "1234567895",
					"1234567896", "1234567897", "1234567898", "1234567899"));

	// This has O(1) time complexity
	public static boolean validate(Message message) {
		return allowedDestinations.contains(message.getTo());
	}
}
